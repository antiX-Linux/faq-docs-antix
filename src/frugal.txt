= Frugal Install
:frugal-page:

{start-toc}

[[toc]]
[compact]
*  link:#under-toc[Introduction]
*  link:#_advantages[Advantages of frugal installations]
*  link:#_description[Description]
*  link:#_description[Definitions]
*  link:#_faq[FAQ]
*  link:#_links[Links]

[[under-toc]]
== Introduction
{top}
image:frugal.jpg"[]

A frugal install goes onto an existing partition and can co-exist with other frugal installs, with installed Linux or installed Windows. 
Yes, you can run antiX frugally on a NTFS (Windows) file system.
The major benefit of a frugal install is that you get to take advantage of the speed and size of an internal drive, up to literally ten times faster! 
If you are starting out with a usb-3 live-usb in a usb-3 port then the speedup will be less dramatic, but compared to booting from a live dvd/cd or usb2, 
be prepared to have your mind blown!
Since antiX-21, it is possible to set up a frugal install on an encrypted drive.

== Advantages of frugal installations:
{top}

Space is saved: installation to hard drive is typically 4 times the size of the ISO, 
whereas with frugal it is the size of the ISO.
Frugal from hard drive is faster than from a LiveMedium.
Great for testing (once set up).
Many antiX users prefer to run a fugal install of antiX.

For most users frugal_static may be the best way to go. For the sizes of the persistence files, with static persistence then 10 GiB 
each should be more than ample. If you use the Live-usb-storage/ directories (which don't even require persistence) for storing 
big things then 1 GiB home should be fine. The size of the root persistence depends on how much you want to be able to add to 
the system before you do a remaster. When you remaster, the root persistence gets combined into the compressed linuxfs file 
so the amount of root persistence used gets reset to close to zero.

== Description
{top}

There are two modes of operation that are triggered by the same frugal boot parameter: install mode and boot mode. Normally 
the frugal device is a partition with the label antiX-Frugal. If such a device is not found then you will be given a list of partitions to 
choose from. Once you have selected a device, you will be given an option to have it be given the antiX-Frugal label. If an antiX frugal 
install has not already been done on the device you selected then a frugal install will be performed.

The next time you boot with the frugal option, it will automatically boot from the antiX-Frugal device into the frugal system that was 
installed. Again, if a frugal install is not found on that device, you will be given the option to automatically create one. If persistence 
is requested and the persistence files are not found then we will offer to create them after we do the install. If you want to force a 
persistence file to be created in a later boot, select that form of persistence with an exclamation point: frugal=root!.

== Definitions
{top}

You will see a number of terms when you select a frugal install that need to be defined.

Frugal installation terms

frugal - Enable frugal mode. Either boot into the existing frugal system or create a new one if it does not already exist.

frugal= - A contraction of frugal persist=*. It is common to add persist options when enabling frugal mode so you can add the persist 
options directly to the frugal boot parameter. Examples: frugal=root or frugal=home.

fdev - The name of the frugal device.

flab - The partition label of the frugal device.

fuuid - The UUID of the frugal partition.

== FAQ
{top}

I am little confused about GRUB2 entry for antiX Frugal. Can I get an example of GRUB2 entry for Frugal install?

Starting with antiX-17, we now make a small "grub.entry" file in the frugal directory that contains the recommended Grub2 
entry to boot into the frugal install.  FWIW, that file is created using this template:

menuentry "$DISTRO_PRETTY_NAME Frugal Install" {
search --no-floppy --set=root --fs-uuid $uuid
linux /$bdir/vmlinuz bdir=$bdir buuid=$uuid $(gather_frugal_cmdline)
initrd /$bdir/initrd.gz
}

== Links
{top}

* {ob}link:https://www.youtube.com/watch?v=6ZGTBUW3bnA[Frugal Install]{cb}.